package io.layback.macadamia;

import io.layback.macadamia.job.Job;
import io.layback.macadamia.job.JobContext;
import io.layback.macadamia.job.JobTransaction;
import io.layback.macadamia.job.SimpleJobContext;
import org.nustaq.net.TCPObjectServer;
import org.nustaq.net.TCPObjectSocket;

import javax.annotation.Nonnull;

/**
 * Fast Serialization client listener for spawning jobs on designated workers when a new client connects.
 *
 * @author Brandon Nesterenko
 */
public class FstClientListener implements TCPObjectServer.NewClientListener {

    private final Worker worker;

    /**
     * Initialize with a worker for completing requested jobs
     *
     * @param worker  the worker
     */
    public FstClientListener(@Nonnull Worker worker) {
        this.worker = worker;
    }

    @Override
    public void connectionAccepted(TCPObjectSocket client) {
        try {
            while (true) {
                Object requestObj = client.readObject();
                client.flush();
                if (requestObj == null || !(requestObj instanceof Job)) {
                    //  TODO log Connection closed if null, bad input if not job
                    return;
                }
                Job job = (Job) requestObj;
                JobTransaction jobTransaction = new FstJobTransaction(client);
                JobContext jobContext = new SimpleJobContext(job, jobTransaction);
                worker.work(jobContext);
            }
        } catch (Exception e) {
            //  TODO
            e.printStackTrace();
        }
    }
}
