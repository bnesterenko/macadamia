package io.layback.macadamia;

import io.layback.macadamia.client.ClientMeta;
import io.layback.macadamia.job.Job;
import io.layback.macadamia.job.JobResponse;
import io.layback.macadamia.job.JobTransaction;
import org.nustaq.net.TCPObjectSocket;

/**
 * Fast Serialization job transaction that holds an open socket for relaying data back to a client
 *
 * @author Brandon Nesterenko
 */
public class FstJobTransaction implements JobTransaction {

    private final TCPObjectSocket socket;

    /**
     * Initialize with an open socket
     *
     * @param socket  the open {@link TCPObjectSocket} connected to a {@link io.layback.macadamia.client.Client}
     */
    public FstJobTransaction(TCPObjectSocket socket) {
        this.socket = socket;
    }

    @Override
    public ClientMeta getClientMeta() {
        return null;
    }

    @Override
    public void respond(JobResponse response) throws Exception {
        socket.writeObject(response);
    }

    @Override
    public void complete() {
        try {
            socket.writeObject(Job.Status.FINISHED);
            socket.close();
        } catch (Exception e) {
            //  tODO
            throw new RuntimeException(e);
        }
    }

    @Override
    public void fail(Throwable cause, String message) {
        try {
            socket.writeObject(Job.Status.FAILED);
            socket.writeObject(message);
        } catch (Exception e) {
            //  TODO
            throw new RuntimeException(e);
        }
    }
}
