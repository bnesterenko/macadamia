package io.layback.macadamia;

import io.layback.macadamia.job.Job;
import org.nustaq.net.TCPObjectSocket;

import java.io.IOException;
import java.util.function.Consumer;

/**
 * Fast Serialization connection to a host
 *
 * @author Brandon Nesterenko
 */
public class FstHostConnection implements NodeConnection {

    private final TCPObjectSocket socket;

    /**
     * Initialize with an open socket for sending messages
     *
     * @param socket  the open {@link TCPObjectSocket} connecting to the node
     */
    public FstHostConnection(TCPObjectSocket socket) {
        this.socket = socket;
    }

    @Override
    public <REQ, RES> void send(REQ request, Consumer<RES> responseConsumer, Class<REQ> requestClass, Class<RES> responseClass)
            throws Exception{
        socket.writeObject(request);
        socket.flush();

        Object response;

        do {
            response = socket.readObject();
            if (response != null && response != Job.Status.FINISHED) {
                responseConsumer.accept(responseClass.cast(response));
            }
        } while (response != Job.Status.FINISHED);
        socket.close();
    }

    @Override
    public void disconnect() throws IOException {
        socket.close();
    }
}
