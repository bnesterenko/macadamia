package io.layback.macadamia;

import io.layback.macadamia.meta.HostMeta;
import org.nustaq.net.TCPObjectSocket;

import java.io.IOException;

/**
 * Fast Serialization connection manager that connects to a {@link org.nustaq.net.TCPObjectServer}
 *
 * @author Brandon Nesterenko
 */
public class FstNodeConnector implements HostNodeConnector {

    /**
     * Connects to a host running an FST TCP Object Server
     *
     * @param hostMeta  the static metadata of the host to connect to
     * @return an open {@link FstHostConnection}
     * @throws IOException  if the connection could not be established
     */
    @Override
    public NodeConnection connect(HostMeta hostMeta) throws IOException {

        TCPObjectSocket socket = new TCPObjectSocket(hostMeta.getHostname(), hostMeta.getPort());

        return new FstHostConnection(socket);
    }
}
