package io.layback.macadamia;

import io.layback.macadamia.job.Job;
import io.layback.macadamia.job.JobDivider;
import io.layback.macadamia.job.JobType;
import io.layback.macadamia.job.SimpleJob;
import io.layback.macadamia.job.SimpleJobResponse;
import io.layback.macadamia.meta.SimpleHostMeta;
import io.layback.macadamia.rt.*;
import io.layback.macadamia.rt.Runtime;
import org.junit.Test;
import org.nustaq.net.TCPObjectServer;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Brandon Nesterenko
 */
public class FstTest {

    @Test
    public void sho2() throws Exception {
        Worker w1 = ctx -> {
            try {
                ctx.getJobTransaction().respond(new SimpleJobResponse<>("Apple"));
                ctx.getJobTransaction().complete();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
        Worker w2 = ctx -> {
            try {
                ctx.getJobTransaction().respond(new SimpleJobResponse<>("Banana"));
                ctx.getJobTransaction().complete();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };

        FstClientListener listener1 = new FstClientListener(w1);
        FstClientListener listener2 = new FstClientListener(w2);

        TCPObjectServer server1 = new TCPObjectServer(5555);
        server1.start(listener1);

        TCPObjectServer server2 = new TCPObjectServer(5556);
        server2.start(listener2);

        Job job = new SimpleJob(TestJobType.INSTANCE, Collections.singletonMap("Keys", Arrays.asList("0", "1")));

        SimpleHostMeta metahost1 = new SimpleHostMeta("localhost", 5555);
        SimpleHostMeta metahost2 = new SimpleHostMeta("localhost", 5556);
        SimpleNodeRuntime nodeRt1 = new SimpleNodeRuntime(metahost1, null, new FstNodeConnector());
        SimpleNodeRuntime nodeRt2 = new SimpleNodeRuntime(metahost2, null, new FstNodeConnector());

        ClusterRuntime crt = new MergeableClusterRuntime(null, null, Arrays.asList(nodeRt1, nodeRt2));

        NodeConnection connection1 = nodeRt1.getNodeConnector().connect(nodeRt1.getNodeMeta());
        NodeConnection connection2 = nodeRt2.getNodeConnector().connect(nodeRt2.getNodeMeta());

        Macadamia.create(crt, new JobDivider() {
            @Override
            public Map<NodeRuntime, Job> divvy(Job inputJob, ClusterRuntime cluster) {
                Collection<String> keys = inputJob.getAttribute("Keys", Collection.class);
                List<Integer> intKeys = keys.stream().map(Integer::parseInt).collect(Collectors.toList());

                List<String> runtimeIds = cluster.getNodes().stream().map(Runtime::getRuntimeId).collect(Collectors.toList());
                Collections.sort(runtimeIds);

                Map<NodeRuntime, Job> divvyJobs = new HashMap<NodeRuntime, Job>();

                runtimeIds.stream().map(cluster::getNode).forEach(key -> divvyJobs.put(key, inputJob));
                return divvyJobs;
            }
        });

        connection1.send(job, res -> {
            System.out.println();
        }, null, SimpleJobResponse.class);
        connection2.send(job, res -> {
            System.out.println();
        }, null, SimpleJobResponse.class);


        System.out.println();
    }

    enum TestJobType implements JobType
    {
        INSTANCE;

        @Override
        public Map<String, Class<?>> getJobAttributes() {
            return Collections.singletonMap("Keys", Collection.class);
        }
    }
}
