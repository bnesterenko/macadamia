package io.layback.macadamia;

import io.layback.macadamia.client.Client;
import io.layback.macadamia.client.ClientMeta;
import io.layback.macadamia.job.Job;
import io.layback.macadamia.job.JobDivider;
import io.layback.macadamia.job.JobResponse;
import io.layback.macadamia.job.NodeResponseConsumer;
import io.layback.macadamia.meta.NodeMeta;
import io.layback.macadamia.rt.*;

import java.util.Map;
import java.util.function.Consumer;

/**
 * Utility class and client for sending asynchronous jobs to a cluster
 *
 * @author Brandon Nesterenko
 */
public final class Macadamia implements Client {

    private final ClusterRuntime clusterRt;
    private final JobDivider jobDivider;

    /**
     * Initialize with the required fields
     *
     * @param clusterRt  the runtime cluster information
     * @param jobDivider  the job divider to distribute a single job among different nodes in the cluster
     */
    public Macadamia(ClusterRuntime clusterRt, JobDivider jobDivider) {
        this.clusterRt = clusterRt;
        this.jobDivider = jobDivider;
    }

    /**
     * Helper class to create an instance
     *
     * @param clusterRuntime  the runtime cluster information
     * @param jobDivider  the job divider to distribute a single job among different nodes in the cluster
     * @return the Macadamia instance client for sending jobs to nodes in the cluster
     */
    public static final Client create(ClusterRuntime clusterRuntime, JobDivider jobDivider) {
        return new Macadamia(clusterRuntime, jobDivider);
    }

    @Override
    public <R> void request(Job job, NodeResponseConsumer<R> responseConsumer, Class<R> responseType) {
        Map<NodeRuntime, Job> divvy = jobDivider.divvy(job, clusterRt);
        divvy.entrySet().forEach(div -> {
            NodeRuntime nodeRt = div.getKey();
            NodeMeta nodeMeta = nodeRt.getNodeMeta();
            try {
                NodeConnection connect = nodeRt.getNodeConnector().connect(nodeMeta);
                Consumer<JobResponse<R>> objConsumer = responseConsumer.getConsumerForRuntime(nodeRt.getRuntimeId());
                connect.send(div.getValue(), objConsumer, Job.class, Macadamia.cast(responseType));
            } catch (Exception e) {
                //  TODO
                throw new RuntimeException(e);
            }
        });
    }

    private static <T> Class<T> cast(Class<?> c) {
        return (Class<T>) c;
    }

    @Override
    public String getRuntimeId() {
        return clusterRt.getRuntimeId();
    }

    @Override
    public ClientMeta getMeta() {
        //  TODO
        return null;
    }

    @Override
    public Map<RuntimeAttribute, RuntimeValue> getKnownAttributes() {
        return clusterRt.getKnownAttributes();
    }

    @Override
    public RuntimeValue getKnownValue(RuntimeAttribute attribute) {
        return clusterRt.getKnownValue(attribute);
    }

    @Override
    public <T> RuntimeValue<T> getKnownValue(RuntimeAttribute attribute, Class<T> tClass) {
        return clusterRt.getKnownValue(attribute, tClass);
    }
}
