package io.layback.macadamia;

import io.layback.macadamia.job.JobContext;

import javax.annotation.Nonnull;

/**
 * Server side worker that completes jobs and relays the response data back to the client
 *
 * @author Brandon Nesterenko
 */
public interface Worker {

    /**
     * Works on a job given its context
     *
     * @param jobContext  the job context that contains the actual job ({@link JobContext#getJob()})
     *                    and job transaction for responding to the client ({@link JobContext#getJobTransaction()})
     */
    void work(@Nonnull JobContext jobContext);
}
