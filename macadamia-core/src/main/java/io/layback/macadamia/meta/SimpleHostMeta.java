package io.layback.macadamia.meta;

import io.layback.macadamia.model.BaseAttributed;

import java.util.HashMap;

/**
 * Simple metadata implementation to provide static information about a host node
 *
 * @author Brandon Nesterenko
 */
public class SimpleHostMeta extends BaseAttributed implements HostMeta {

    /**
     * Initialize with the required fields
     *
     * @param hostname  the hostname
     * @param port  the port
     */
    public SimpleHostMeta(final String hostname, final int port) {
        super(new HashMap<String, Object>() {
            {
                put(HostAttributes.HOSTNAME.name(), hostname);
                put(HostAttributes.PORT.name(), port);
            }
        });
    }

    @Override
    public String getHostname() {
        return getAttribute(HostAttributes.HOSTNAME.name(), String.class);
    }

    @Override
    public int getPort() {
        return getAttribute(HostAttributes.PORT.name(), Integer.class);
    }
}
