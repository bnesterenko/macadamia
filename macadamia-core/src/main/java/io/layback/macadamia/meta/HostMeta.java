package io.layback.macadamia.meta;

/**
 * Metadata holder to store static host information
 * <p/>
 * @author Brandon Nesterenko
 */
public interface HostMeta extends NodeMeta
{
    /**
     * Gets the name or IP address of the associated host
     *
     * @return  the name or IP address of the host
     */
    String getHostname();

    /**
     * Gets the port of the associated host
     *
     * @return  the port
     */
    int getPort();

    /**
     * Universally supported attributes for a host
     */
    enum HostAttributes
    {
        HOSTNAME,
        PORT
    }
}
