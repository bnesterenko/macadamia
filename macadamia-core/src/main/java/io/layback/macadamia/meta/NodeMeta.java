package io.layback.macadamia.meta;

/**
 * Metadata specific to a node in a cluster
 *
 * @author Brandon Nesterenko
 */
public interface NodeMeta extends Meta {
}
