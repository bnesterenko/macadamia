package io.layback.macadamia.meta;

import io.layback.macadamia.model.BaseAttributed;

/**
 * Simple implementation of metadata specific to a cluster of nodes
 *
 * @author Brandon Nesterenko
 */
public class SimpleClusterMeta extends BaseAttributed implements ClusterMeta {

    /**
     * Default initializer to set all attributes to null
     */
    public SimpleClusterMeta() {
        super(null);
    }
}
