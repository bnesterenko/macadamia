package io.layback.macadamia.meta;

/**
 * Metadata holder to store static cluster information
 *
 * @author Brandon Nesterenko
 */
public interface ClusterMeta extends Meta {
}
