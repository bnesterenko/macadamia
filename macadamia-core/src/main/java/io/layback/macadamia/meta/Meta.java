package io.layback.macadamia.meta;

import io.layback.macadamia.model.Attributed;

/**
 * Static metadata applied to a node.
 * <br>
 * <p>
 * All metadata fields and attributes should be completely immutable.
 * Any fields subject to changing at runtime should be implemented within the {@link io.layback.macadamia.rt.Runtime}
 * equivalent of this metadata.
 *
 * @author Brandon Nesterenko
 */
public interface Meta extends Attributed {
}
