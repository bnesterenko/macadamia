package io.layback.macadamia;

import io.layback.macadamia.meta.HostMeta;

/**
 * Connection manager for interacting with hosts
 *
 * @author Brandon Nesterenko
 */
public interface HostNodeConnector extends NodeConnector<HostMeta> {
}
