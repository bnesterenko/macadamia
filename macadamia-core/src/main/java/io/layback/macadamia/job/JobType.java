package io.layback.macadamia.job;

import java.util.Map;

/**
 * A holder that categorizes a job so it can be divided or worked on appropriately
 * <p>
 * Implementing sub classes should be handled and distributed correctly by a {@link JobDivider} or {@link io.layback.macadamia.Worker}
 *
 * @author Brandon Nesterenko
 */
public interface JobType {

    /**
     * Gets the definition for each attribute required by the actual job
     *
     * @return all attributes required by the job
     */
    Map<String, Class<?>> getJobAttributes();
}
