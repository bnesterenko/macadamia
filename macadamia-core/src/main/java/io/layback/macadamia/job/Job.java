package io.layback.macadamia.job;

import io.layback.macadamia.model.Attributed;

/**
 * A job to be executed by workers
 *
 * A job consists of a {@link JobType} and series of attributes to create a surrounding context
 *
 * @author Brandon Nesterenko
 */
public interface Job extends Attributed {

    /**
     * Gets the job type that specifies the category of this job
     *
     * @return the categorizing job type
     */
    JobType getJobType();

    /**
     * Possible status values of a job
     */
    enum Status {
        FINISHED,
        FAILED
    }
}
