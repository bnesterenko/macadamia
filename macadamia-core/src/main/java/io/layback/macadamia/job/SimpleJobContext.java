package io.layback.macadamia.job;

/**
 * Simple job context to provide only the basic functionality to workers
 *
 * @author Brandon Nesterenko
 */
public class SimpleJobContext implements JobContext {

    private final Job job;
    private final JobTransaction jobTransaction;


    /**
     * Initialize with the required fields
     *
     * @param job  the job for a worker to complete
     * @param jobTransaction  the job transaction that keeps an open connection with the client
     */
    public SimpleJobContext(Job job, JobTransaction jobTransaction) {
        this.job = job;
        this.jobTransaction = jobTransaction;
    }

    @Override
    public Job getJob() {
        return job;
    }

    @Override
    public JobTransaction getJobTransaction() {
        return jobTransaction;
    }
}
