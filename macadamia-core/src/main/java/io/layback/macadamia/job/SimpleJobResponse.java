package io.layback.macadamia.job;

import io.layback.macadamia.model.BaseAttributed;

import javax.annotation.Nullable;
import java.util.Map;

/**
 * Simple job response that holds the intended return object as well as a map of attributes
 *
 * @author Brandon Nesterenko
 *
 * @param <R>  the response type returned by {@code #getValue()}
 */
public class SimpleJobResponse<R> extends BaseAttributed implements JobResponse<R> {

    private final R value;

    /**
     * Initialize just with a return value
     *
     * @param value  the return value
     */
    public SimpleJobResponse(R value) {
        this.value = value;
    }

    /**
     * Initialize with all basic fields
     *
     * @param value  the return value for this response
     * @param attributes  a map of attributes describing the return value
     */
    public SimpleJobResponse(R value, @Nullable Map<String, Object> attributes)
    {
        super(attributes);
        this.value = value;
    }

    @Override
    public R get() {
        return value;
    }
}
