package io.layback.macadamia.job;

import java.util.function.Consumer;

/**
 * A delegator to lookup a job response consumer for given runtime id
 *
 * @author Brandon Nesterenko
 *
 * @param <ID>  the type of the runtime id
 * @param <R>  the value of the object stored within each job response consumed
 */
public interface ResponseConsumer<ID, R> {

    /**
     * Gets the job response consumer for the particular runtime id
     *
     * @param runtimeId  the runtime id to lookup for returning the job response consumer
     * @return the job response consumer for the runtime
     */
    Consumer<JobResponse<R>> getConsumerForRuntime(ID runtimeId);
}
