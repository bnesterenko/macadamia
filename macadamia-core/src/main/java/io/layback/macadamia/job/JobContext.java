package io.layback.macadamia.job;

/**
 * A worker-side job context that provides workers with their assigned job and functionality to respond back to a client
 *
 * @author Brandon Nesterenko
 */
public interface JobContext {

    /**
     * A job for a worker to complete alongside all attributes
     *
     * @return  the job
     */
    Job getJob();

    /**
     * An open transaction to the client for relaying data in a stream
     *
     * @return the job transaction
     */
    JobTransaction getJobTransaction();
}
