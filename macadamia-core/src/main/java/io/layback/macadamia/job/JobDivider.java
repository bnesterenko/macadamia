package io.layback.macadamia.job;

import io.layback.macadamia.rt.ClusterRuntime;
import io.layback.macadamia.rt.NodeRuntime;

import javax.annotation.Nonnull;
import java.util.Map;

/**
 * A divider to divvy out a single job into multiple sub jobs assigned to a particular node
 *
 * @author Brandon Nesterenko
 */
public interface JobDivider {

    /**
     * Divvies out the original job to nodes within a cluster
     *
     * @param inputJob  the original job to divide out to nodes
     * @param cluster  the cluster to divide the work of the job
     * @return a map where the nodes are keys that point to the sub job
     */
    @Nonnull
    Map<NodeRuntime, Job> divvy(@Nonnull Job inputJob, @Nonnull ClusterRuntime cluster);
}
