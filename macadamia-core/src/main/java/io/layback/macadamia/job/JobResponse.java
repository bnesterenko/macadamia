package io.layback.macadamia.job;

import io.layback.macadamia.model.Attributed;

/**
 * A response provided by a worker that is sent back to a client
 * <p/>
 * @author Brandon Nesterenko
 *
 * @param <R> the type of data contained within this response
 */
public interface JobResponse<R> extends Attributed
{

    /**
     * Get the response object contained within this response
     *
     * @return the response object
     */
    R get();
}
