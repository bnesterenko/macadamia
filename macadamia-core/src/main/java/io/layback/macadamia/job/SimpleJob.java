package io.layback.macadamia.job;

import io.layback.macadamia.model.BaseAttributed;

import javax.annotation.Nullable;
import java.util.Map;

/**
 * Simple job implementation that only holds a the job type and attributes
 *
 * @author Brandon Nesterenko
 */
public class SimpleJob extends BaseAttributed implements Job {

    private final JobType jobType;

    /**
     * Initialize the job
     *
     * @param jobType  the job type categorizing this job
     * @param attributes  additional attributes to pass along with this job. Note that his
     *                    must contain at least the attributes described in
     *                    {@link JobType#getJobAttributes()}
     */
    public SimpleJob(JobType jobType, @Nullable Map<String, Object> attributes) {
        super(attributes);
        this.jobType = jobType;
    }

    @Override
    public JobType getJobType() {
        return jobType;
    }
}
