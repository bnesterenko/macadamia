package io.layback.macadamia.job;

import io.layback.macadamia.client.ClientMeta;

/**
 * @author Brandon Nesterenko
 */
public interface JobTransaction {
    ClientMeta getClientMeta();

    void respond(JobResponse response) throws Exception;

    void complete();

    void fail(Throwable cause, String message);
}
