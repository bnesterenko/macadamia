package io.layback.macadamia.job;

/**
 * A holder for getting job response consumers from a node
 *
 * @author Brandon Nesterenko
 *
 * @param <R>  the value of the object stored within each job response consumed
 */
public interface NodeResponseConsumer<R> extends ResponseConsumer<String, R> {
}
