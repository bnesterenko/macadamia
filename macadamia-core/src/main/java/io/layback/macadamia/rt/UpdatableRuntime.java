package io.layback.macadamia.rt;

import io.layback.macadamia.meta.Meta;

/**
 * Indicator that a runtime component is able to be updated at real-time
 *
 * @author Brandon Nesterenko
 *
 * @param <ID>  the runtime id type of the component  returned by {@link Runtime#getRuntimeId()}
 * @param <META>  the {@link Meta} type of the runtime component
 */
public interface UpdatableRuntime<ID, META extends Meta> extends Runtime<ID, META> {
    void updateRuntimeValue(RuntimeAttribute updatedAttribute, RuntimeValue updatedValue);
}
