package io.layback.macadamia.rt;

import io.layback.macadamia.meta.Meta;

import java.util.Collections;
import java.util.Map;
import java.util.UUID;

/**
 * Default strategy that supplies the functionality for updating runtime attributes for runtime components
 *
 * @author Brandon Nesterenko
 *
 * @param <META>  the type of Meta for the underlying runtime component
 */
public abstract class DefaultUpdatableRuntime<META extends Meta> implements UpdatableRuntime<String, META> {

    private final String uuid;
    private final META meta;
    private final Map<RuntimeAttribute, RuntimeValue> runtimeAttributes;

    private final Object lock = new Object();

    /**
     * Initializes with the required fields
     *
     * @param meta  the metadata for the runtime component
     * @param runtimeAttributes  the runtime attributes for the component
     */
    public DefaultUpdatableRuntime(META meta, Map<RuntimeAttribute, RuntimeValue> runtimeAttributes) {
        this.meta = meta;
        this.runtimeAttributes = runtimeAttributes;
        uuid = UUID.randomUUID().toString();
    }

    @Override
    public String getRuntimeId() {
        return uuid;
    }

    @Override
    public META getMeta() {
        return meta;
    }

    @Override
    public Map<RuntimeAttribute, RuntimeValue> getKnownAttributes() {
        return Collections.unmodifiableMap(runtimeAttributes);
    }

    @Override
    public RuntimeValue getKnownValue(RuntimeAttribute attribute) {
        return runtimeAttributes.get(attribute);
    }

    @Override
    public <T> RuntimeValue<T> getKnownValue(RuntimeAttribute attribute, Class<T> tClass) {
        return (RuntimeValue<T>) getKnownValue(attribute);
    }

    @Override
    public void updateRuntimeValue(RuntimeAttribute updatedAttribute, RuntimeValue updatedValue) {
        synchronized (lock) {
            if (runtimeAttributes != null && runtimeAttributes.containsKey(updatedAttribute)) {
                RuntimeValue runtimeValue = runtimeAttributes.get(updatedAttribute);
                if (updatedValue.asOf().isAfter(runtimeValue.asOf())) {
                    //  This is a newer value, so use it
                    runtimeAttributes.put(updatedAttribute, updatedValue);
                }
            }
        }
    }
}
