package io.layback.macadamia.rt;

import io.layback.macadamia.meta.ClusterMeta;
import io.layback.macadamia.model.Mergeable;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * A cluster component that may be merged with other clusters
 *
 * @author Brandon Nesterenko
 */
public class MergeableClusterRuntime extends DefaultUpdatableRuntime<ClusterMeta> implements ClusterRuntime, Mergeable<ClusterRuntime> {

    private final List<String> runtimeOrdering;
    private final Map<String, NodeRuntime> nodes;

    private final Object lock = new Object();

    /**
     * Initializes with the required fields
     *
     * @param clusterMeta  the cluster metadata for this cluster. May be null
     * @param runtimeAttributes  the runtime attributes for the cluster. May be null
     * @param nodes  the initial nodes held within this cluster. Should not be null
     */
    public MergeableClusterRuntime(@Nullable  ClusterMeta clusterMeta, @Nullable Map<RuntimeAttribute, RuntimeValue> runtimeAttributes,
                                   Collection<NodeRuntime> nodes) {
        super(clusterMeta, runtimeAttributes);
        if (nodes != null) {
            runtimeOrdering = new CopyOnWriteArrayList<>();
            this.nodes = new ConcurrentHashMap<>();
            nodes.forEach(this::addNode);
        } else {
            runtimeOrdering = null;
            this.nodes = null;
        }
    }

    @Override
    public Collection<NodeRuntime> getNodes() {
        return Collections.unmodifiableCollection(nodes.values());
    }

    @Override
    public NodeRuntime getNode(String nodeId) {
        return nodes.get(nodeId);
    }

    @Override
    public void mergeWith(ClusterRuntime other) {
        Collection<NodeRuntime> otherNodes = other.getNodes();
        if (otherNodes != null) {
            otherNodes.forEach(this::addNode);
        }
    }

    /**
     * Adds a new node to this cluster
     *
     * @param node  the node to add to this cluster
     */
    protected void addNode(NodeRuntime node) {
        String id = node.getRuntimeId();
        synchronized (lock) {
            if (!nodes.containsKey(id)) {
                nodes.put(id, node);
                runtimeOrdering.add(id);
                Collections.sort(runtimeOrdering);
            }
        }
    }
}
