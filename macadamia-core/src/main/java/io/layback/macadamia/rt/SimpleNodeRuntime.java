package io.layback.macadamia.rt;

import io.layback.macadamia.NodeConnector;
import io.layback.macadamia.meta.NodeMeta;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

/**
 * Basic holder for runtime information describing the current state of a remote node
 *
 * @author Brandon Nesterenko
 */
public class SimpleNodeRuntime extends DefaultUpdatableRuntime<NodeMeta> implements NodeRuntime {

    private final NodeMeta nodeMeta;
    private final NodeConnector nodeConnector;

    /**
     * Initializes with the required fields
     *
     * @param nodeMeta  the corresponding static metadata for the node
     * @param runtimeAttributes  the initial runtime attributes for this node. Can be null and defaults will be used
     * @param nodeConnector  the strategy that handles connections with this node
     */
    public SimpleNodeRuntime(@Nonnull NodeMeta nodeMeta, @Nullable Map<RuntimeAttribute, RuntimeValue> runtimeAttributes,
                             @Nonnull NodeConnector nodeConnector) {
        super(nodeMeta, new HashMap<RuntimeAttribute, RuntimeValue>(){
            {
                for (NodeRtAttribute rtAttribute : NodeRtAttribute.values()) {
                    if (runtimeAttributes != null && runtimeAttributes.containsKey(rtAttribute)) {
                        //  RT attribute is explicitly set, use it
                        put(rtAttribute, runtimeAttributes.get(rtAttribute));
                    } else {
                        //  This runtime value was not explicitly set, use the default value
                        put(rtAttribute, new SimpleRuntimeValue<>(rtAttribute.getDefaultValue(), rtAttribute.getDefaultValueClass()));
                    }
                }

            }
        });
        this.nodeMeta = nodeMeta;
        this.nodeConnector = nodeConnector;
    }

    @Override
    public NodeMeta getNodeMeta() {
        return nodeMeta;
    }

    @Override
    public NodeConnector getNodeConnector() {
        return nodeConnector;
    }
}
