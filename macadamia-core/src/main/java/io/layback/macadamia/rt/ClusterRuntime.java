package io.layback.macadamia.rt;

import io.layback.macadamia.meta.ClusterMeta;

import java.util.Collection;

/**
 * The runtime data stored for a cluster
 *
 * @author Brandon Nesterenko
 */
public interface ClusterRuntime extends Runtime<String, ClusterMeta> {

    /**
     * Gets the nodes within this cluster
     *
     * @return the nodes within this cluster
     */
    Collection<NodeRuntime> getNodes();


    /**
     * Gets a node within this cluster by its runtime identifier
     *
     * @param nodeId  the runtime identifier for the node to get
     * @return the node with the corresponding runtime id
     */
    NodeRuntime getNode(String nodeId);
}
