package io.layback.macadamia.rt;

import java.time.Instant;

/**
 *  Holder for values that correspond to runtime attributes for components
 *
 * @author Brandon Nesterenko
 */
public interface RuntimeValue<V> {

    /**
     * Gets the timestamp that this value was set
     *
     * @return the timestamp this value was set
     */
    Instant asOf();

    /**
     * Gets the actual value for the runtime attribute
     *
     * @return the value
     */
    V getValue();

    /**
     * The class of the value
     *
     * @return the class of the value
     */
    Class<V> getValueClass();
}
