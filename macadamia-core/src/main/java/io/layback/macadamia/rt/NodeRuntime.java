package io.layback.macadamia.rt;

import com.sun.org.apache.xpath.internal.operations.Bool;
import io.layback.macadamia.NodeConnection;
import io.layback.macadamia.NodeConnector;
import io.layback.macadamia.meta.NodeMeta;

import java.util.Collection;

/**
 * @author Brandon Nesterenko
 */
public interface NodeRuntime extends Runtime<String, NodeMeta> {
    NodeMeta getNodeMeta();

    NodeConnector getNodeConnector();

    enum NodeRtAttribute implements RuntimeAttribute<Boolean> {
        ALIVE(true);

        private final boolean defaultValue;

        NodeRtAttribute(boolean defaultValue) {
            this.defaultValue = defaultValue;
        }

        @Override
        public Boolean getDefaultValue() {
            return defaultValue;
        }

        @Override
        public Class<Boolean> getDefaultValueClass() {
            return Boolean.class;
        }
    }

    enum NodeStatus {
        
    }
}
