package io.layback.macadamia.rt;

import java.time.Instant;

/**
 * Broadcasted update to all nodes within a cluster to indicate a new or dead node
 * <p>
 * This is currently unimplemented; however, eventually it could be used to allow work stealing between
 * nodes
 *
 * @author Brandon Nesterenko
 *
 * @param <ID>  the type of runtime identifier used by {@link Runtime#getRuntimeId()}
 */
public interface RuntimeUpdate<ID> {
    ID getRuntimeId();

    RuntimeAttribute getUpdatedAttribute();

    RuntimeValue getUpdatedValue();

    Instant getTimeOfUpdate();
}
