package io.layback.macadamia.rt;

import io.layback.macadamia.meta.Meta;

import javax.annotation.Nonnull;
import java.util.Map;

/**
 * Broadcaster to communicate runtime updates to all nodes within a cluster
 * <p>
 * This is currently unimplemented; however, eventually it could be used to allow work stealing between
 * nodes
 *
 * @author Brandon Nesterenko
 *
 * @param <R>
 * @param <ID>
 * @param <META>
 */
public interface RuntimeUpdater<R extends Runtime<ID, META>, ID, META extends Meta> {
    void updateAttributes(@Nonnull R runtime, Map<RuntimeAttribute, Object> attributeUpdates);

    void updateAttribute(@Nonnull R runtime, RuntimeAttribute attribute, Object newValue);
}
