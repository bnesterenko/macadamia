package io.layback.macadamia.rt;

import io.layback.macadamia.meta.Meta;

import java.util.Map;

/**
 * Provider of dynamic runtime information about a component
 * <p>
 * This class provides real-time information that corresponds to a static {@link Meta} component.
 * <p>
 * @author Brandon Nesterenko
 *
 * @param <ID>   the type of unique identifier that this component is associated with at runtime
 * @param <META> the type of meta that this runtime provides real-time data about
 */
public interface Runtime<ID, META extends Meta> {

    /**
     * Gets the unique runtime identifier for this component
     *
     * @return the runtime identifier
     */
    ID getRuntimeId();

    /**
     * Gets the static metadata for this component
     *
     * @return the static metadata
     */
    META getMeta();

    /**
     * Gets all attributes that this component is known to be.
     * <p>
     * These attributes are subject to change based on real-time information.
     *
     * @return the map of known runtime attributes
     */
    Map<RuntimeAttribute, RuntimeValue> getKnownAttributes();

    /**
     * Looks up a single attribute that this component is known to be.
     *
     * @param attribute  the attribute to look up
     * @return the current known value of the attribute
     */
    RuntimeValue getKnownValue(RuntimeAttribute attribute);

    /**
     * Looks up a single attribute by name and expected type that this component is known to be
     *
     * @param attribute  the attribute to look up
     * @param tClass  the expected class type to be returned by the {@link RuntimeValue#getValue()}
     * @param <T>  the generic expected class type of the returned {@link RuntimeValue#getValueClass()}
     * @return  the {@code RuntimeValue} holding the current known value for the attribute
     */
    <T> RuntimeValue<T> getKnownValue(RuntimeAttribute attribute, Class<T> tClass);
}
