package io.layback.macadamia.rt;

/**
 * Declare official attribute types allowed to be accessed/updated at runtime for components
 *
 * @author Brandon Nesterenko
 *
 * @param <T>  the expected value type for this attribute, along with the default value to be used if none is supplied
 */
public interface RuntimeAttribute<T> {
    /**
     * Gets the name of the attribute
     *
     * @return the name
     */
    String name();

    /**
     * Gets the default value of this runtime component.
     * <p>
     * If a {@link Runtime} component is instantiated without this attribute, it will be automatically populated
     * with this default value
     *
     * @return the default attribute value
     */
    T getDefaultValue();

    /**
     * Gets the class of the default value
     *
     * @return the class of the default value
     */
    Class<T> getDefaultValueClass();
}
