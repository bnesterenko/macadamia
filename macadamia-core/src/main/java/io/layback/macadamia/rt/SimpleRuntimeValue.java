package io.layback.macadamia.rt;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.time.Instant;

/**
 * Simply holds a value corresponding to the current state of a runtime attribute on a component
 * <p>
 * Instances of this class should not be cached, as the runtime value pointed at by a {@link RuntimeAttribute}
 * may change.
 *
 * @author Brandon Nesterenko
 *
 * @param <V>  the expected class type for the value
 */
@Immutable
public class SimpleRuntimeValue<V> implements RuntimeValue<V> {

    private final Instant asOf;
    private final V value;
    private final Class<V> valueClass;

    /**
     * Convenience initializer where the timestamp of this value is set to the time this
     * instance is created
     *
     * @param value  the value
     * @param valueClass  the class of the value
     */
    public SimpleRuntimeValue(@Nullable V value, @Nullable Class<V> valueClass) {
        this(Instant.now(), value, valueClass);
    }

    /**
     * Manual initializer
     *
     * @param asOf  the timestamp this value was initially set
     * @param value  the value
     * @param valueClass  the class of the value
     */
    public SimpleRuntimeValue(Instant asOf, V value, Class<V> valueClass) {
        this.asOf = asOf;
        this.value = value;
        this.valueClass = valueClass;
    }

    @Override
    public Instant asOf() {
        return asOf;
    }

    @Override
    public V getValue() {
        return value;
    }

    @Override
    public Class<V> getValueClass() {
        return valueClass;
    }
}
