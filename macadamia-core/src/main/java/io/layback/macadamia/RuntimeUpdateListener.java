package io.layback.macadamia;

import io.layback.macadamia.meta.NodeMeta;
import io.layback.macadamia.rt.Runtime;
import io.layback.macadamia.rt.RuntimeAttribute;
import io.layback.macadamia.rt.RuntimeUpdate;

/**
 * @author Brandon Nesterenko
 */
public interface RuntimeUpdateListener {
    void notify(RuntimeUpdate runtimeUpdate);
}
