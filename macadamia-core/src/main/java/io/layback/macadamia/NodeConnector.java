package io.layback.macadamia;

import io.layback.macadamia.meta.NodeMeta;

import java.io.IOException;

/**
 * Connection manager for creating an open node connection to a runtime component
 *
 * @author Brandon Nesterenko
 *
 * @param <M>  the {@link io.layback.macadamia.meta.Meta} to use for connecting to the component
 */
public interface NodeConnector<M extends NodeMeta> {

    /**
     * Connects to the runtime component
     *
     * @param nodeMeta  the metadata to use for establishing a connection
     * @return an open {@link NodeConnection} for relaying requests to the node
     * @throws IOException if the connection could not be established
     */
    NodeConnection connect(M nodeMeta) throws IOException;
}
