package io.layback.macadamia;

import java.util.function.Consumer;

/**
 * Connection to a worker node that allows sending requests
 *
 * @author Brandon Nesterenko
 */
public interface NodeConnection extends AutoCloseable {
    /**
     * Sends a request to a worker node and calls the response consumer whenever the worker responds with new data
     *
     * @param request  the request to send to the worker node
     * @param responseConsumer  the response consumer to call when the worker node responds with data
     * @param requestClass  the class of the request
     * @param responseClass  the class of the expected response from a worker node
     * @param <REQ>  generic request class
     * @param <RES>  generic response class
     * @throws Exception  if the request failed to send to the worker node
     */
    <REQ, RES> void send(REQ request, Consumer<RES> responseConsumer, Class<REQ> requestClass, Class<RES> responseClass) throws Exception;

    /**
     * Disconnects from the worker node.
     * <p>
     * Connections should never be closed by the worker.
     * The response consumer should close the connection to the node when the job has finished
     *
     * @throws Exception  if disconnection fails
     */
    void disconnect() throws Exception;

    @Override
    default void close() throws Exception {
        this.disconnect();
    }
}
