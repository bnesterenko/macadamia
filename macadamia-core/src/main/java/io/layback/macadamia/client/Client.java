package io.layback.macadamia.client;

import io.layback.macadamia.job.Job;
import io.layback.macadamia.job.NodeResponseConsumer;
import io.layback.macadamia.rt.Runtime;

/**
 * A client for sending jobs to a  cluster of workers.
 *
 * @author Brandon Nesterenko
 */
public interface Client extends Runtime<String, ClientMeta> {

    /**
     * Send an asynchronous job to the workers within the configured clusters
     *
     * @param job  the job for the workers to finish
     * @param responseConsumer  a holder to lookup a job consumer specific to each node in the cluster
     * @param responseType  the expected response type that is returned from each node
     * @param <R>  the response type
     */
    <R> void request(Job job, NodeResponseConsumer<R> responseConsumer, Class<R> responseType);
}
