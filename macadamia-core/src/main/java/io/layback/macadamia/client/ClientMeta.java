package io.layback.macadamia.client;

import io.layback.macadamia.meta.Meta;

/**
 * Metadata specific to a runtime client
 *
 * @author Brandon Nesterenko
 */
public interface ClientMeta extends Meta {
}
