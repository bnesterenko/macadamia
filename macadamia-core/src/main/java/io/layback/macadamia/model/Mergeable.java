package io.layback.macadamia.model;

/**
 * Indicate that a bean may be merged together with another bean of a specified type
 *
 * @author Brandon Nesterenko
 *
 * @param <T>  the type of class that this instance may be merged with
 */
public interface Mergeable<T> {

    /**
     * Performs a merge with another bean of the specified type
     *
     * @param other  the other bean to merge this bean with. The passed in parameter instance must remain
     *               unchanged after the merge.
     */
    void mergeWith(T other);
}
