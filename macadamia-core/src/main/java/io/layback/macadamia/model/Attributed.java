package io.layback.macadamia.model;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;

/**
 * Indicate that a bean should allow generic attributes to provide extra information
 *
 * @author Brandon Nesterenko
 */
public interface Attributed {

    /**
     * Gets the map of all attributes
     *
     * @return  the map of all attributes
     */
    @Nullable
    Map<String, Object> getAttributes();

    /**
     * Gets a specific attribute value by name
     *
     * @param attrName  the attribute name to lookup
     * @return  the corresponding value of the attribute
     */
    @Nullable
    Object getAttribute(@Nonnull String attrName);

    /**
     * Gets a specific attribute value by name and expected type
     *
     * @param attrName  the attribute name to lookup
     * @param tClass  the expected class type to be returned
     * @param <T>  the generic expected class type to be returned
     * @return  the typed corresponding value of the attribute
     */
    @Nullable
    <T> T getAttribute(@Nonnull String attrName, Class<T> tClass);
}
