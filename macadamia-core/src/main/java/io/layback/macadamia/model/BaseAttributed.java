package io.layback.macadamia.model;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.Map;

/**
 * Abstract class that provides any subclasses with the attributed functionality
 *
 * @author Brandon Nesterenko
 */
public abstract class BaseAttributed implements Attributed, Serializable {
    private final Map<String, Object> attributes;

    /**
     * Initializes with no (null) attributes
     */
    public BaseAttributed() {
        attributes = null;
    }

    /**
     * Initializes with predefined attributes
     *
     * @param attributes  the map of attributes to initialize with
     */
    public BaseAttributed(@Nullable Map<String, Object> attributes) {
        this.attributes = attributes;
    }


    @Override
    public Map<String, Object> getAttributes() {
        return attributes;
    }

    @Override
    public Object getAttribute(@Nonnull String attrName) {
        Object obj = null;

        if (attributes != null && attrName != null) {
            obj = attributes.get(attrName);
        }

        return obj;
    }

    @Override
    public <T> T getAttribute(@Nonnull String attrName, Class<T> tClass) {
        T tObj = null;

        Object obj = getAttribute(attrName);
        if (obj != null) {
            tObj = tClass.cast(obj);
        }

        return tObj;
    }
}
