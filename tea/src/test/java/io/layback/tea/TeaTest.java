package io.layback.tea;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import io.layback.macadamia.FstClientListener;
import io.layback.macadamia.FstNodeConnector;
import io.layback.macadamia.client.Client;
import io.layback.macadamia.job.JobResponse;
import io.layback.macadamia.job.SimpleJob;
import io.layback.macadamia.meta.SimpleHostMeta;
import io.layback.macadamia.rt.MergeableClusterRuntime;
import io.layback.macadamia.rt.SimpleNodeRuntime;
import org.junit.Test;
import org.nustaq.net.TCPObjectServer;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author Brandon Nesterenko
 */
public class TeaTest {

    @Test
    public void sho3() throws IOException {
        //  Node 1 setup
        LoadingCache<String, BeanClass> cache1 = Caffeine.newBuilder()
            .build(key -> new BeanClass(key + " : " + key.hashCode(), 1));
        DefaultCacheLookup lookup1 = new DefaultCacheLookup(
            Collections.singletonMap(new DefaultCacheLookup.ClassTuple(String.class, BeanClass.class), cache1));
        CacheDistributorWorker worker1 = new CacheDistributorWorker(lookup1);
        FstClientListener listener1 = new FstClientListener(worker1);
        TCPObjectServer server1 = new TCPObjectServer(5555);
        server1.start(listener1);
        SimpleHostMeta host1 = new SimpleHostMeta("localhost", 5555);
        SimpleNodeRuntime nodeRt1 = new SimpleNodeRuntime(host1, null, new FstNodeConnector());

        //  Node 2 setup
        LoadingCache<String, BeanClass> cache2 = Caffeine.newBuilder()
            .build(key -> new BeanClass(key + " : " + key.hashCode(), 2));
        DefaultCacheLookup lookup2 = new DefaultCacheLookup(
                Collections.singletonMap(new DefaultCacheLookup.ClassTuple(String.class, BeanClass.class), cache2));
        CacheDistributorWorker worker2 = new CacheDistributorWorker(lookup2);
        FstClientListener listener2 = new FstClientListener(worker2);
        TCPObjectServer server2 = new TCPObjectServer(5556);
        server2.start(listener2);
        SimpleHostMeta host2 = new SimpleHostMeta("localhost", 5556);
        SimpleNodeRuntime nodeRt2 = new SimpleNodeRuntime(host2, null, new FstNodeConnector());

        //  Set up the client
        MergeableClusterRuntime clusterRt = new MergeableClusterRuntime(null, null, Arrays.asList(nodeRt1, nodeRt2));
        Client client = Tea.create(clusterRt);

        //  Create & send the job to the nodes
        List<String> keys = IntStream.range(1, 500).mapToObj(String::valueOf).collect(Collectors.toList());
        Map<String, Object> jobAttributes = new HashMap<>();
        jobAttributes.put(CacheJobType.CacheAttribute.KEY_CLASS.name(), String.class);
        jobAttributes.put(CacheJobType.CacheAttribute.VALUE_CLASS.name(), BeanClass.class);
        jobAttributes.put(CacheJobType.CacheAttribute.KEYS.name(), keys);
        SimpleJob job = new SimpleJob(CacheJobType.INSTANCE, jobAttributes);
        client.request(job, runtimeId ->
            jr -> {
            System.out.println();
        }, JobResponse.class);
    }

    public static class BeanClass implements Serializable {
        private final String name;
        private final int age;

        public BeanClass(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public int getAge() {
            return age;
        }
    }
}
