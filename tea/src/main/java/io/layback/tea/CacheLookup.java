package io.layback.tea;

import com.github.benmanes.caffeine.cache.LoadingCache;

/**
 * Container for holding caches.
 * <p>
 * Each cache is indexed by the key and value type stored within the {@link com.github.benmanes.caffeine.cache.Cache}
 * This is useful if a cluster is caching multiple data types
 *
 * @author Brandon Nesterenko
 */
public interface CacheLookup {
    /**
     * Finds the stored loading cache by its key and value type
     *
     * @param keyClass  the class of the keys within the cache
     * @param valueClass  the class of the values within the cache
     * @param <K>  generic key type
     * @param <V>  generic value type
     * @return  the {@link LoadingCache} of generic types {@code K} and {@code V}
     */
    <K, V> LoadingCache<K, V> getLoadingCache(Class<K> keyClass, Class<V> valueClass);
}
