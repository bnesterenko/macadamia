package io.layback.tea;

import io.layback.macadamia.Macadamia;
import io.layback.macadamia.client.Client;
import io.layback.macadamia.rt.ClusterRuntime;

/**
 * Create a client that accesses the distributed cache
 *
 * @author Brandon Nesterenko
 */
public class Tea
{

    /**
     * Creates the client based on the configured runtime
     *
     * @param clusterRt  the runtime cluster that distributes cache entries to the configured nodes
     * @return the client
     */
    public static Client create(ClusterRuntime clusterRt)
    {
        return new Macadamia(clusterRt, new DefaultCacheJobDivider());
    }
}
