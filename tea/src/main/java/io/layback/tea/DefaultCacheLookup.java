package io.layback.tea;

import com.github.benmanes.caffeine.cache.LoadingCache;

import java.util.Map;
import java.util.Objects;

/**
 * Cache lookup that indexes loading caches by their key and value types
 *
 * @author Brandon Nesterenko
 */
public class DefaultCacheLookup implements CacheLookup {

    private final Map<ClassTuple, LoadingCache> cacheMap;

    public DefaultCacheLookup(Map<ClassTuple, LoadingCache> cacheMap) {
        this.cacheMap = cacheMap;
    }

    @Override
    public <K, V> LoadingCache<K, V> getLoadingCache(Class<K> keyClass, Class<V> valueClass) {

        ClassTuple<K, V> classTuple = new ClassTuple<>(keyClass, valueClass);
        return cacheMap.get(classTuple);
    }

    /**
     * Holder for the key and value types stored by an implementing loading cache
     *
     * @param <K>  the generic key type
     * @param <V>  the generic value type
     */
    public static class ClassTuple<K, V> {
        private final Class<K> keyClass;
        private final Class<V> valueClass;

        /**
         * Initialize with the key and value types
         *
         * @param keyClass  the key type
         * @param valueClass  the value type
         */
        public ClassTuple(Class<K> keyClass, Class<V> valueClass) {
            this.keyClass = keyClass;
            this.valueClass = valueClass;
        }

        public Class<K> getKeyClass() {
            return keyClass;
        }

        public Class<V> getValueClass() {
            return valueClass;
        }

        @Override
        public int hashCode() {
            return Objects.hash(keyClass, valueClass);
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof ClassTuple)) {
                return false;
            }

            ClassTuple otherClassTuple = (ClassTuple) obj;

            return Objects.equals(keyClass, otherClassTuple.getKeyClass())
                    && Objects.equals(valueClass, otherClassTuple.getValueClass());
        }
    }
}
