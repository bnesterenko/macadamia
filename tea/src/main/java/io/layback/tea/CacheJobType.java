package io.layback.tea;

import io.layback.macadamia.job.JobType;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Job type indicating a job is targetted toward the cache
 *
 * @author Brandon Nesterenko
 */
public enum CacheJobType implements JobType {
    INSTANCE;

    private static final Map<String, Class<?>> attributes = new HashMap<String, Class<?>>() {
        {
            for (CacheAttribute cacheAttribute : CacheAttribute.values()) {
                put(cacheAttribute.name(), cacheAttribute.type());
            }
        }
    };

    @Override
    public Map<String, Class<?>> getJobAttributes() {
        return attributes;
    }

    /**
     * Required attributes to be in a {@link io.layback.macadamia.job.Job} for accessing cached data
     */
    enum CacheAttribute {
        KEY_CLASS(Class.class),
        VALUE_CLASS(Class.class),
        KEYS(Collection.class);

        private final Class<?> type;

        CacheAttribute(Class<?> type) {
            this.type = type;
        }

        public Class<?> type() {
            return type;
        }
    }
}
