package io.layback.tea;

import io.layback.macadamia.job.Job;

/**
 * Filters individual values returned from a cache
 *
 * @author Brandon Nesterenko
 *
 * @param <T>  the type of values this class will be filtering
 */
public interface ValueFilter<T> {

    /**
     * Gets the class for the values this class will be filtering
     *
     * @return  the class for the values to be filtered
     */
    Class<T> getValueClass();

    /**
     * Filters a single entry in a cache.
     *
     * @param input  the input value returned by the cache
     * @param inputJob  the job that may hold contextual attributes to aid in filtering (classifications, etc)
     * @return true if the input value should be returned to the {@link io.layback.macadamia.client.Client}, false if it should be taken out and not
     *         sent back to the client.
     */
    boolean filterValue(T input, Job inputJob);
}
