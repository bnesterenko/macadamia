package io.layback.tea;

import com.github.benmanes.caffeine.cache.LoadingCache;
import io.layback.macadamia.Worker;
import io.layback.macadamia.job.Job;
import io.layback.macadamia.job.JobContext;
import io.layback.macadamia.job.SimpleJobResponse;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Server side worker that finds information in a cache and relays the data back to the client
 *
 * @author Brandon Nesterenko
 */
public class CacheDistributorWorker implements Worker {

    private final CacheLookup cacheLookup;
    private final ValueFilterLookup valueFilterLookup;

    /**
     * Initialize with a cache holder for looking up based on key and value class type
     *
     * @param cacheLookup  the cache lookup
     */
    public CacheDistributorWorker(@Nonnull CacheLookup cacheLookup) {
        this(cacheLookup, null);
    }

    /**
     * Initialize with all fields
     *
     * @param cacheLookup  a cache holder for looking up a {@link com.github.benmanes.caffeine.cache.Cache} based on
     *                     request key and value type
     * @param valueFilterLookup  a value filtering strategy that blocks data from being sent back to a client
     */
    public CacheDistributorWorker(@Nonnull CacheLookup cacheLookup, @Nullable ValueFilterLookup valueFilterLookup) {
        this.cacheLookup = cacheLookup;
        this.valueFilterLookup = valueFilterLookup;
    }

    @Override
    public void work(@Nonnull JobContext jobContext) {
        Job job = jobContext.getJob();
        if (!(job.getJobType() instanceof CacheJobType)) {
            throw new IllegalArgumentException("Cannot distribute non cache job types");
        }

        Class keyClass = job.getAttribute(CacheJobType.CacheAttribute.KEY_CLASS.name(), Class.class);
        Class valueClass = job.getAttribute(CacheJobType.CacheAttribute.VALUE_CLASS.name(), Class.class);
        Collection<String> keys = job.getAttribute(CacheJobType.CacheAttribute.KEYS.name(), Collection.class);
        LoadingCache loadingCache = cacheLookup.getLoadingCache(keyClass, valueClass);
        Map allEntries = loadingCache.getAll(keys);


        Map filteredEntries;
        if (valueFilterLookup != null) {
            ValueFilter valueFilter = valueFilterLookup.getValueFilter(valueClass);
            if (valueFilter != null) {
                filteredEntries = new HashMap<>();
                allEntries.keySet().stream().filter(key -> valueFilter.filterValue(allEntries.get(key), job))
                        .forEach(key -> filteredEntries.put(key, allEntries.get(key)));
            } else {
                filteredEntries = allEntries;
            }
        } else {
            filteredEntries = allEntries;
        }

        try {
            jobContext.getJobTransaction().respond(new SimpleJobResponse<>(filteredEntries));
            jobContext.getJobTransaction().complete();
        } catch (Exception e) {
            jobContext.getJobTransaction().fail(e, e.getMessage());
        }
    }
}
