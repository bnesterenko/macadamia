package io.layback.tea;

import io.layback.macadamia.job.Job;
import io.layback.macadamia.job.JobDivider;
import io.layback.macadamia.job.SimpleJob;
import io.layback.macadamia.rt.ClusterRuntime;
import io.layback.macadamia.rt.NodeRuntime;
import io.layback.macadamia.rt.Runtime;
import io.layback.tea.hash.DefaultKeyHasher;
import io.layback.tea.hash.KeyHasher;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Job divider that distributes keys based on the first character in the key as a string.
 * <p>
 * The key type must override {@link Object#toString()} for this to be meaningful
 *
 * @author Brandon Nesterenko
 */
public class DefaultCacheJobDivider implements JobDivider
{

    private final KeyHasher hashStrategy;

    /**
     * Initialize with the default key hashing strategy {@link DefaultKeyHasher}
     */
    public DefaultCacheJobDivider() {
        this(DefaultKeyHasher.INSTANCE);
    }

    /**
     * Initialize with a custom key hashing strategy for distributing key lookups to different worker nodes
     *
     * @param hashStrategy  the custom key hashing strategy
     */
    public DefaultCacheJobDivider(KeyHasher hashStrategy) {
        this.hashStrategy = hashStrategy;
    }

    @Nonnull
    @Override
    public Map<NodeRuntime, Job> divvy(@Nonnull Job inputJob, @Nonnull ClusterRuntime cluster) {
        Objects.requireNonNull(inputJob);
        Objects.requireNonNull(cluster);

        if (!(inputJob.getJobType() instanceof CacheJobType)) {
            throw new IllegalArgumentException("Cannot divvy cache job for non cache job type");
        }

        Collection<NodeRuntime> nodes = cluster.getNodes();
        List<String> nodeIds = nodes.stream().map(Runtime::getRuntimeId).collect(Collectors.toList());
        Collections.sort(nodeIds);

        Collection keys = inputJob.getAttribute(CacheJobType.CacheAttribute.KEYS.name(), Collection.class);

        int numNodes = nodes.size();
        Map<Integer, Collection<String>> dividedKeys = new HashMap<>(numNodes);

        keys.stream().forEach(key -> {
            int nodeIdx = hashStrategy.hash(key) % numNodes;
            if (!dividedKeys.containsKey(nodeIdx)) {
                //  Make a guess that says each node will receive an equal amount of keys to process
                dividedKeys.put(nodeIdx, new ArrayList<>(keys.size() / numNodes));
            }
            dividedKeys.get(nodeIdx).add(String.class.cast(key));
        });

        Map<NodeRuntime, Job> dividedJobs = new HashMap<>(numNodes);

        dividedKeys.entrySet().stream().forEach(nodeIdxEntry -> {
            Integer nodeIdx = nodeIdxEntry.getKey();
            String nodeRtKey = nodeIds.get(nodeIdx);
            NodeRuntime nodeRt = cluster.getNode(nodeRtKey);
            Map<String, Object> dividedJobAttributes = new HashMap<>();
            inputJob.getAttributes().keySet().stream().filter(key -> !key.equals(CacheJobType.CacheAttribute.KEYS.name()))
                    .forEach(key -> dividedJobAttributes.put(key, inputJob.getAttribute(key)));
            dividedJobAttributes.put(CacheJobType.CacheAttribute.KEYS.name(), nodeIdxEntry.getValue());
            SimpleJob dividedJob = new SimpleJob(CacheJobType.INSTANCE, dividedJobAttributes);
            dividedJobs.put(nodeRt, dividedJob);
        });

        return dividedJobs;
    }
}
