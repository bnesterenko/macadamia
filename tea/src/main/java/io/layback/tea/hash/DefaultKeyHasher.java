package io.layback.tea.hash;

/**
 * @author Brandon Nesterenko
 */
public enum DefaultKeyHasher implements KeyHasher {
    INSTANCE;

    @Override
    public int hash(Object toHash) {
        if (toHash == null) {
            return 0;
        } else {
            return toHash.toString().charAt(0);
        }
    }
}
