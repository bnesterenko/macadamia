package io.layback.tea.hash;

/**
 * @author Brandon Nesterenko
 */
public interface KeyHasher {
    int hash(Object toHash);
}
