package io.layback.tea;

/**
 * @author Brandon Nesterenko
 */
public interface ValueFilterLookup {
    <V> ValueFilter<V> getValueFilter(Class<V> valueClass);
}
